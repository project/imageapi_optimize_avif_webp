<?php

namespace Drupal\imageapi_optimize_avif_webp\Plugin\ImageAPIOptimizeProcessor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\imageapi_optimize\ConfigurableImageAPIOptimizeProcessorBase;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\File\FileSystemInterface;

/**
 * AVIF & WebP Deriver.
 *
 * @ImageAPIOptimizeProcessor(
 *   id = "imageapi_optimize_avif_webp",
 *   label = @Translation("AVIF & WebP Deriver"),
 *   description = @Translation("Clone image to AVIF or WebP")
 * )
 */
class AvifWebp extends ConfigurableImageAPIOptimizeProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function applyToImage($image_uri) {

    $path_info = pathinfo($image_uri);
    // If .avif or webp file then do nothing.
    if ($path_info['extension'] == 'avif' || $path_info['extension'] == 'webp') {
      return FALSE;
    }
    $toolkit_id = $this->imageFactory->getToolkitId();
    $source_image = $this->imageFactory->get($image_uri, $toolkit_id);
    $avif_quality = $this->configuration['avif_quality'];
    $webp_quality = $this->configuration['webp_quality'];
    $extensionless_image_uri = \Drupal::service('imageapi_optimize_avif_webp.helper')->extensionlessUri($image_uri);

    if ($source_image) {

      /*********** Webp image ****************/
      $destination = $extensionless_image_uri . '.webp';
      // Check if ImageMagick supports webp.
      if ($toolkit_id == 'imagemagick' && in_array('webp', $this->imageFactory->getSupportedExtensions())) {
        try {
          $source_image->apply('convert', ['extension' => 'webp', 'quality' => $webp_quality]);
          $source_image->save($destination);

          // Fix issue where sometimes image fails to generate.
          if (filesize($destination) % 2 == 1) {
            file_put_contents($destination, "\0", FILE_APPEND);
          }

          //return TRUE;
        }
        catch (\Exception $error) {
          // Something else went wrong, unrelated to the Tinify API.
          $this->logger->error('AVIF: Failed to optimize image using ImageMagick due to "%error".', ['%error' => $error->getMessage()]);
        }
      }
      // Go with GD.
      else {
        imagewebp($source_image->getToolkit()->getResource(), $destination, $webp_quality);
        // Fix issue where sometimes image fails to generate.
        if (filesize($destination) % 2 == 1) {
          file_put_contents($destination, "\0", FILE_APPEND);
        }
      }
      /*********** Avif image ****************/
      $destination = $extensionless_image_uri . '.avif';
      // Check if ImageMagick supports avif.
      if ($toolkit_id == 'imagemagick' && in_array('avif', $this->imageFactory->getSupportedExtensions())) {
        try {
          $source_image->apply('convert', ['extension' => 'avif', 'quality' => $avif_quality]);
          $source_image->save($destination);

          // Fix issue where sometimes image fails to generate.
          if (filesize($destination) % 2 == 1) {
            file_put_contents($destination, "\0", FILE_APPEND);
          }

          //return TRUE;
        }
        catch (\Exception $error) {
          // Something else went wrong, unrelated to the Tinify API.
          $this->logger->error('AVIF: Failed to optimize image using ImageMagick due to "%error".', ['%error' => $error->getMessage()]);
        }
      }
      // Go with GD.
      else {
        try {
          imageavif(
            $source_image->getToolkit()->getResource(),
            $destination,
            $avif_quality
          );

          // Fix issue where sometimes image fails to generate.
          if (filesize($destination) % 2 == 1) {
            file_put_contents($destination, "\0", FILE_APPEND);
          }

          //return TRUE;
        }
        catch (\Exception $error) {
          // Something else went wrong, unrelated to the Tinify API.
          $this->logger->error('AVIF: Failed to optimize image using GD due to "%error".', ['%error' => $error->getMessage()]);
        }
      }

    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'avif_quality' => 60,
      'webp_quality' => 60,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // @todo: Add ability to pick which image types allow derivatives.
    $form['avif_quality'] = [
      '#type' => 'number',
      '#title' => $this->t('AVIF Image quality'),
      '#description' => $this->t('Specify the image quality.'),
      '#default_value' => $this->configuration['avif_quality'],
      '#required' => TRUE,
      '#min' => 1,
      '#max' => 100,
    ];
    $form['webp_quality'] = [
      '#type' => 'number',
      '#title' => $this->t('WebP Image quality'),
      '#description' => $this->t('Specify the image quality.'),
      '#default_value' => $this->configuration['webp_quality'],
      '#required' => TRUE,
      '#min' => 1,
      '#max' => 100,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['avif_quality'] = $form_state->getValue('avif_quality');
    $this->configuration['webp_quality'] = $form_state->getValue('webp_quality');

  }

}
