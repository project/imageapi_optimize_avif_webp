<?php
namespace Drupal\imageapi_optimize_avif_webp\Entity;

use Drupal\imageapi_optimize\Entity\ImageAPIOptimizePipeline;
use Drupal\Core\File\FileSystemInterface;

/**
 * Wrap ImageAPIOptimizePipeline to copy AVIF & WebP derivative to proper directory.
 *
 * This wrapper allows for .avif & .webp image derivatives to be copied
 * to the correct directory after the AVIF & WebP image_api handler takes place.
 *
 * Class ImageAPIOptimizeAvifWebpPipeline
 *
 * @package Drupal\imageapi_optimize_avif_webp\Entity
 *
 * @param \Drupal\Core\File\FileSystemInterface $filesystem
 */
class ImageAPIOptimizeAvifWebpPipeline extends ImageAPIOptimizePipeline {

  /**
   * {@inheritdoc}
   */
  public function applyToImage($image_uri) {
    parent::applyToImage($image_uri);

    // If the source file doesn't exist, return FALSE.
    $image = \Drupal::service('image.factory')->get($image_uri);
    $extensionless_image_service = \Drupal::service('imageapi_optimize_avif_webp.helper');
    if (!$image->isValid()) {
      return FALSE;
    }

    $extensionless_image_uri = $extensionless_image_service->extensionlessUri($image_uri);

    if (count($this->getProcessors())) {
      $avif_uri = $extensionless_image_uri . '.avif';
	    $webp_uri = $extensionless_image_uri . '.webp';

	    // Generating avif image.
      foreach ($this->temporaryFiles as $temp_image_uri) {
        $extensionless_temp_image_uri = $extensionless_image_service->extensionlessUri($temp_image_uri);
        $temp_avif_uri = $extensionless_temp_image_uri . '.avif';
        if (file_exists($temp_avif_uri)) {
          \Drupal::service('file_system')->move($temp_avif_uri, $avif_uri, FileSystemInterface::EXISTS_REPLACE);
        }
	  }

	  // Generating webp image.
	  foreach ($this->temporaryFiles as $temp_image_uri) {
        $extensionless_temp_image_uri = $extensionless_image_service->extensionlessUri($temp_image_uri);
        $temp_webp_uri = $extensionless_temp_image_uri . '.webp';
        if (file_exists($temp_webp_uri)) {
          \Drupal::service('file_system')->move($temp_webp_uri, $webp_uri, FileSystemInterface::EXISTS_REPLACE);
        }
      }
    }
  }

}

