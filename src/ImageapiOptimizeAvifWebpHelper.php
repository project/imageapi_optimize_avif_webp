<?php

namespace Drupal\imageapi_optimize_avif_webp;

/**
 * Provides a helper service with useful methods for the module.
 */
class ImageapiOptimizeAvifWebpHelper {

  /**
   * Returns URI of an image without extension part.
   *
   * @param string $original_uri
   *   Original image URI.
   *
   * @return string
   *   URI of the image without extension part.
   */
  public function extensionlessUri(string $original_uri): string {
	  return $original_uri;
    
  }

}
